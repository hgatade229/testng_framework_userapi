Endpoint is :
https://dummyjson.com/users/add

Request Body is :
{
    "firstName": "Nikhil",
    "lastName": "Verma",
    "age": "27.0"
}

Executed on Date is :
Fri, 08 Mar 2024 10:40:32 GMT

Response Body  is :
{"id":101,"firstName":"Nikhil","lastName":"Verma","maidenName":"","age":"27.0","gender":"","email":"","phone":"","username":"","password":"","birthDate":"","image":"","bloodGroup":"","height":null,"weight":null,"eyeColor":"","hair":{"color":"","type":""},"domain":"","ip":"","address":{"address":"","city":"","coordinates":{"lat":null,"lng":null},"postalCode":"","state":""},"macAddress":"","university":"","bank":{"cardExpire":"","cardNumber":"","cardType":"","currency":"","iban":""},"company":{"address":{"address":"","city":"","coordinates":{"lat":null,"lng":null},"postalCode":"","state":""},"department":"","name":"","title":""},"ein":"","ssn":"","userAgent":""}

