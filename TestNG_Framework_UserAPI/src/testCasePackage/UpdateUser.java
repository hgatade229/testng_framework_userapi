package testCasePackage;
import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import commonMethod.Api_Trigger;
import commonMethod.Utility;
import repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class UpdateUser {

	@Test
	public static void executor() throws IOException
	{



		File dir_name = Utility.CreateLogDir("Api_Logs");

		String requestBody  = RequestBody.updateBody("update_tc2");

		String endpoint = RequestBody.Hostname() + RequestBody.UpdateUserResource();

		int statuscode = 0;



		for (int i = 0; i < 5; i++) {
			Response response = Api_Trigger.UpdateUserApi(RequestBody.Headername(), RequestBody.Headervalue(), requestBody, endpoint);


			statuscode = response.statusCode();
			

			if (statuscode == 200) {
				Utility.CreateEvidanceFile("updateApi", dir_name, endpoint, requestBody, 
						response.getHeader("Date"), response.getBody().asString());
				Validator(response, requestBody);
				break;
			}
			else {
				System.out.println("Expected status code is not found in current iteration :" +(i+1)+ " retrying");
			}

		}

		if (statuscode!=200) 
		{
			System.out.println("Expected status code not found even after 5 retries hence failing the test case");
			Assert.assertEquals(statuscode, 200);
		}


	}


	public static void Validator(Response response,String requestBody)
	{

		JsonPath j_req = new JsonPath(requestBody);


		Assert.assertEquals(j_req.getString("lastName"), response.jsonPath().getString("lastName"));


	}
}