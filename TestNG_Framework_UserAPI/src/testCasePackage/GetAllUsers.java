package testCasePackage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.Test;

import commonMethod.Api_Trigger;
import commonMethod.Utility;
import repository.RequestBody;
import io.restassured.response.Response;

public class GetAllUsers {
	
	    @Test
	    public static void executor() throws IOException
	    {
	    	
			   File dir_name = Utility.CreateLogDir("Api_Logs");

	    	
	    	String endpoint = RequestBody.Hostname() + RequestBody.GetAllUsersResource();
	    	Response response = Api_Trigger.GetUserApi(RequestBody.Headername(), RequestBody.Headervalue(), endpoint);
	    	
	    	
	    	
	    	Utility.GetUsersEvidanceFile("getAllUsers", dir_name, response.getBody().asPrettyString());
	    	
	        /* int totalUsers = response.getBody().jsonPath().getString("users").length(); 

	    	System.out.println(totalUsers);
	    	//System.out.println(response.getBody().asPrettyString());

    		ArrayList<String> arrayData = new ArrayList<String>();

	    	
	    	for(int i=0;i<10;i++)
	    	{
	    	   
	    		String user = response.getBody().jsonPath().getString("users[i]");
	    		arrayData.add(user);
	    		
	    	}
	    	
	    	System.out.println(arrayData); */

	    	
	    }

}