package testCasePackage;
import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import commonMethod.Api_Trigger;
import commonMethod.Utility;
import repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class CreateUserUsingAnnotation {

	static int statuscode = 0;

	static String endpoint;

	static String requestBody;

	static File dir_name;

	static Response response;


	@BeforeTest
	public static void TriggerSetup() throws IOException
	{
		
		System.out.println("Before Test has run");
		
		dir_name = Utility.CreateLogDir("Api_Logs");

		requestBody  = RequestBody.createBody("create_tc2");

		endpoint = RequestBody.Hostname() + RequestBody.CreateUserResource();

	}


	@Test
	public static void executor() throws IOException
	{

		System.out.println(" Test has run");

		for (int i = 0; i < 5; i++) {
			response = Api_Trigger.CreateUserApi(RequestBody.Headername(), RequestBody.Headervalue(), requestBody, endpoint);


			statuscode = response.statusCode();

			if (statuscode == 200) {
				JsonPath j_req = new JsonPath(requestBody);

				//System.out.println(j_req.getString("firstName"));

				Assert.assertEquals(j_req.getString("firstName"), response.jsonPath().getString("firstName"));
				Assert.assertEquals(j_req.getString("lastName"), response.jsonPath().getString("lastName"));
				Assert.assertEquals(j_req.getString("age"), response.jsonPath().getString("age"));
				Assert.assertNotNull(response.jsonPath().getString("age"));
				break;
			}
			else {
				System.out.println("Expected status code is not found in current iteration :" +(i+1)+ " retrying");
			}

		}

		if (statuscode!=200) {
			System.out.println("Expected status code not found even after 5 retries hence failing the test case");
			Assert.assertEquals(statuscode, 200);
		}

           
		
	}




	@AfterTest
	public static void EvidanceFileCreator() throws IOException
	{

		
		System.out.println("After Test has run");

		Utility.CreateEvidanceFile("createApi", dir_name, endpoint, requestBody, 
				response.getHeader("Date"), response.getBody().asString());
	}
}