package testCasePackage;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import commonMethod.Api_Trigger;
import commonMethod.Utility;
import repository.RequestBody;
import io.restassured.response.Response;

public class DeleteUser {
          
	@Test
	public static void executor() throws IOException
	{
		  
		   File dir_name = Utility.CreateLogDir("Api_Logs");

		  String endpoint = RequestBody.Hostname() + RequestBody.DeleteUserResource("1");
		  Response response = Api_Trigger.DeleteUserApi(RequestBody.Headername(), RequestBody.Headervalue(), endpoint);
		  
          Utility.DeleteUsersEvidanceFile("deleteApi", dir_name, endpoint, response.getBody().asPrettyString());		  
		  //System.out.println(response.getBody().asPrettyString());
		  
	}
}